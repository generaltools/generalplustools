//
// Created by ngejz7 on 2024-04-26.
//

#include "AbaqusDatabaseOps.h"
#include <iostream>
#include "sqlite3.h"
#include <string>

class SimulationDatabase
{
protected:
    sqlite3* db3;

    virtual int openDatabase(const std::string& db3Name)
    {
        return sqlite3_open(db3Name.c_str(), &db3);
    }

    virtual int closeDatabase()
    {
        return sqlite3_close(db3);
    }

    virtual int executeQuery(const std::string& query, int (*callback)(void*, int, char**, char**), void* data)
    {
        char* errMsg = nullptr;
        return sqlite3_exec(db3, query.c_str(), callback, data, &errMsg);
    }

public:
    virtual int createTable() = 0;
    virtual int insertData() = 0;
    virtual int selectData() = 0;

    int processDatabase(const std::string& db3Name)
    {
        int retVal = SQLITE_OK;

        if (retVal == SQLITE_OK)
        {
            retVal = openDatabase(db3Name);
        }
        if (retVal == SQLITE_OK)
        {
            retVal = createTable();
        }
        if (retVal == SQLITE_OK)
        {
            retVal = insertData();
        }
        if (retVal == SQLITE_OK)
        {
            retVal = selectData();
        }
        if (retVal == SQLITE_OK)
        {
            retVal = closeDatabase();
        }

        return retVal;
    }

};


class AbaqusDatabaseConstructor : SimulationDatabase
{

};