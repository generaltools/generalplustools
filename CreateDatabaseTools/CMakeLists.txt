## Adding Library of Abaqus Tools
add_library(CreateDatabaseTools_Lib STATIC
        AbaqusModelDatabaseDefinition.cpp
)

## Including Headers
target_include_directories(CreateDatabaseTools_Lib PUBLIC "./")