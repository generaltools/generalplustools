//
// Created by ngejz7 on 2024-04-26.
//

#include "AbaqusModelDatabaseDefinition.h"


std::string AbaqusModelDatabaseDefinition::NodeIndexing()
{
    std::string nodeIndexing = "CREATE UNIQUE INDEX NODE_ID_INDEX ON Node_Coordinates(NODE_ID)";

    return nodeIndexing;
}

std::string AbaqusModelDatabaseDefinition::ElementNodeIndexing()
{
    std::string elementNodeIndexing = "CREATE INDEX ELEMENT_NODE_INDEX ON Element_Nodes(Node)";

    return elementNodeIndexing;
}

std::string AbaqusModelDatabaseDefinition::ElementIndex()
{
    std::string elementIndex = "CREATE INDEX ELEMENT_INDEX ON Element_Nodes(Element)";

    return elementIndex;
}

std::string AbaqusModelDatabaseDefinition::EntityIndex()
{
    std::string entityIndex = "CREATE INDEX ENTITY_INDEX ON EntityDef(SetID)";

    return entityIndex;
}


std::string AbaqusModelDatabaseDefinition::NodeCoordinatesDef()
{

    std::string nodeCoords = "CREATE TABLE IF NOT EXISTS Node_Coordinates("
                             "NODE_ID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                             "CoordX REAL NOT NULL, "
                             "CoordY REAL NOT NULL, "
                             "CoordZ REAL NOT NULL)";

    return nodeCoords;
}

std::string AbaqusModelDatabaseDefinition::ElementNodesDef()
{
    std::string elementNodes = "CREATE TABLE IF NOT EXISTS Element_Nodes("
            "ElementNode_ID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
            "Element INTEGER NOT NULL, "
            "orderID INTEGER NOT NULL, "
            "Node INTEGER NOT NULL, "
            "SetID INTEGER NOT NULL, "
            "FOREIGN KEY(Node) REFERENCES Node_Coordinates(NODE_ID), "
            "FOREIGN KEY(SetID) REFERENCES SetDef(SetID), "
            "CONSTRAINT ElementUnicity UNIQUE(ElementNode_ID, Element, orderID, Node, SetID) ON CONFLICT FAIL)";

    return elementNodes;
}

std::string AbaqusModelDatabaseDefinition::CouplingDef()
{
    std::string couplingDef = "CREATE TABLE IF NOT EXISTS CouplingDef("
                              "Coupling_ID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                              "ConstraintName TEXT NOT NULL, "
                              "Ref_Node INTEGER NOT NULL, "
                              "surfaceName TEXT NOT NULL, "
                              "coupling_Type TEXT NOT NULL, "
                              "DoF TEXT NOT NULL, "
                              "FOREIGN KEY(Ref_Node) REFERENCES Node_Coordinates(NODE_ID), "
                              "CONSTRAINT couplingUnicity UNIQUE(Coupling_ID, ConstraintName, Ref_Node, coupling_Type) ON CONFLICT FAIL)";

    return couplingDef;
}

std::string AbaqusModelDatabaseDefinition::SurfaceDef()
{
    std::string surfaceDef = "CREATE TABLE IF NOT EXISTS SurfaceDef("
                             "Surface_ID INTEGER UNIIQUE NOT NULL PRIMARY KEY, "
                             "SurfaceName TEXT NOT NULL, "
                             "SurfaceType TEXT NOT NULL, "
                             "element INTEGER, "
                             "SurfaceSide TEXT, "
                             "node INTEGER,"
                             "NodeWeightiing REAL, "
                             "FOREIGN KEY(Node) REFERENCES Node_Coordinates(NODE_ID))";

    return surfaceDef;
}

std::string AbaqusModelDatabaseDefinition::SetDef()
{
    std::string setDef = "CREATE TABLE IF NOT EXISTS SetDef("
                         "SetID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                         "setName TEXT NOT NULL, "
                         "SetType TEXT NOT NULL, "
                         "CONSTRAINT setUnicity UNIQUE(SetID, setName, SetType) ON CONFLICT FAIL)";

    return setDef;
}

std::string AbaqusModelDatabaseDefinition::EntityDef()
{
    std::string EntityDef = "CREATE TABLE IF NOT EXISTS EntityDef("
                            "Entity_ID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                            "entity TEXT NOT NULL, "
                            "SetID INTEGER NOT NULL, "
                            "FOREIGN KEY(SetID) REFERENCES SetDef(SetID), "
                            "CONSTRAINT entityUnicity UNIQUE(entity, SetID) ON CONFLICT FAIL)";

    return EntityDef;
}

std::string AbaqusModelDatabaseDefinition::SectionDef()
{
    std::string sectionDef = "CREATE TABLE IF NOT EXISTS SectionDef("
                             "SectionID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                             "Section TEXT NOT NULL, "
                             "Setname TEXT NOT NULL, "
                             "materialName TEXT NOT NULL, "
                             "FOREIGN KEY(materialName) REFERENCES Mat_ElasticDef(MaterialName), "
                             "CONSTRAINT sectionUnicity UNIQUE(SectionID, Section, setName) ON CONFLICT FAIL)";

    return sectionDef;
}

std::string AbaqusModelDatabaseDefinition::CompSectionDef()
{
    std::string compSectionDef = "CREATE TABLE IF NOT EXISTS CompDef("
                                 "compSectionID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                                 "setName TEXT NOT NULL, "
                                 "sectionType TEXT NOT NULL, "
                                 "materialName TEXT NOT NULL, "
                                 "shellThickness REAL, "
                                 "FOREIGN KEY(compSectionID) REFERENCES SectionDef(SectionID), "
                                 "CONSTRAINT compsec_Unicity UNIQUE(compSectionID, setName, sectionType, materialName) On CONFLICT FAIL)";

    return compSectionDef;
}

std::string AbaqusModelDatabaseDefinition::BeamSectionDef()
{
    std::string beamSectionDef = "CREATE TABLE IF NOT EXISTS BeamSectionDef("
                                 "beamSectionID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                                 "setName TEXT NOT NULL, "
                                 "materialName TEXT NOT NULL, "
                                 "sectionType TEXT NOT NULL, "
                                 "Dimensions TEXT, "
                                 "DirCos1 REAL, "
                                 "DirCos2 REAL, "
                                 "DirCos3 REAL, "
                                 "FOREIGN KEY(beamSectionID) REFERENCES SectionDef(SectionID), "
                                 "CONSTRAINT beamsec_Unicity UNIQUE(beamSectionID, setName, sectionType, materialName) ON CONFLICT FAIL)";

    return beamSectionDef;
}

std::string AbaqusModelDatabaseDefinition::ConnectorDef()
{
    std::string connectorDef = "CREATE TABLE IF NOT EXISTS ConnectorDef("
                               "connSectionID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                               "setName TEXT NOT NULL, "
                               "behaviour_ID INTEGER, "
                               "controlsname TEXT, "
                               "eliminationSetting BOOL, "
                               "connectorType TEXT NOT NULL, "
                               "orientationName TEXT, "
                               "FOREIGN KEY(connSectionID) REFERENCES SectionDef(SectionID), "
                               "FOREIGN KEY(behaviour_ID) REFERENCES BehaviourDef(BehaviourID), "
                               "CONSTRAINT connector_Unicity UNIQUE(connSectionID, setName, behaviour_ID, connectorType) ON CONFLICT FAIL)";

    return connectorDef;
}

std::string AbaqusModelDatabaseDefinition::BehaviourDef()
{
    std::string behaviourDef = "CREATE TABLE IF NOT EXISTS BehaviourDef("
                               "BehaviourID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                               "behaviourName TEXT NOT NULL, "
                               "Elast_K1 REAL, "
                               "Elast_K2 REAL, "
                               "Elast_K3 REAL, "
                               "Elast_K4 REAL, "
                               "Elast_K5 REAL, "
                               "Elast_K6 REAL, "
                               "CONSTRAINT behaviour_Unicity UNIQUE(BehaviourID, behaviourName) ON CONFLICT FAIL)";

    return behaviourDef;
}
std::string AbaqusModelDatabaseDefinition::MassDef()
{
    std::string massDef = "CREATE TABLE IF NOT EXISTS MassDef("
                          "MassID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                          "setName TEXT NOT NULL, "
                          "weight REAL NOT NULL,"
                          "CONSTRAINT mass_Unicity UNIQUE(MassID, setName, weight) ON CONFLICT FAIL)";

    return massDef;
}

std::string AbaqusModelDatabaseDefinition::MaterialElasticDef()
{
    std::string materialElasticDef = "CREATE TABLE IF NOT EXISTS Mat_ElasticDef("
                                     "MatID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                                     "MaterialName TEXT UNIQUE NOT NULL, "
                                     "Elastic_type TEXT, "
                                     "ElasticNod REAL, "
                                     "ElasticPoiss REAL, "
                                     "ElasticTemp REAL, "
                                     "Density REAL, "
                                     "ElasticMod_1 REAL, "
                                     "ElasticMod_2 REAL, "
                                     "ElasticPoiss_12 REAL, "
                                     "ShearMod_12 REAL, "
                                     "ShearMod_13 REAL, "
                                     "ShearMod_23 REAL)";

    return materialElasticDef;
}

std::string AbaqusModelDatabaseDefinition::MaterialPlasticDef()
{
    std::string materialPlasticDef = "CREATE TABLE IF NOT EXISTS Mat_PlasticDef("
                                     "PlasticID INTEGER UNIQUE NOT NULL PRIMARY KEY, "
                                     "Hardening TEXT, "
                                     "stress_ly REAL, "
                                     "strain_plast REAL, "
                                     "Temperature_plast REAL, "
                                     "MatID INTEGER NOT NULL, "
                                     "FOREIGN KEY(MatID) REFERENCES Mat_ElasticDef(MatID))";

    return materialPlasticDef;
}

std::string AbaqusModelDatabaseDefinition::SupportingFilesDef()
{
    std::string supportingFilesDef = "CREATE TABLE IF NOT EXISTS Supporting_Files("
                                      "name TEXT NOT NULL, "
                                      "full_path TEXT NOT NULL, "
                                      "extension TEXT NOT NULL)";

    return supportingFilesDef;
}

