//
// Created by ngejz7 on 2024-04-26.
//

#include <iostream>


#ifndef GENERALPLUSTOOLS_ABAQUSMODELDATABASEDEFINITION_H
#define GENERALPLUSTOOLS_ABAQUSMODELDATABASEDEFINITION_H


class AbaqusModelDatabaseDefinition
{
public:
    // Table Definitions
    static std::string NodeCoordinatesDef();
    static std::string ElementNodesDef();
    static std::string CouplingDef();
    static std::string SurfaceDef();
    static std::string SetDef();
    static std::string EntityDef();
    static std::string SectionDef();
    static std::string CompSectionDef();
    static std::string BeamSectionDef();
    static std::string ConnectorDef();
    static std::string BehaviourDef();
    static std::string MassDef();
    static std::string MaterialElasticDef();
    static std::string MaterialPlasticDef();
    static std::string SupportingFilesDef();

    // Indexing Definitions
    static std::string NodeIndexing();
    static std::string ElementNodeIndexing();
    static std::string ElementIndex();
    static std::string EntityIndex();


};


#endif //GENERALPLUSTOOLS_ABAQUSMODELDATABASEDEFINITION_H
