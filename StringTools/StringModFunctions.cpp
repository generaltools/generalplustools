//
// Created by ngejz7 on 2024-04-23.
//

#include "StringModFunctions.h"

#include <vector>
#include <iostream>
#include <algorithm>

// Function to remove a character value from the existing string - Returns a string without the inserted delimiter
std::string RemoveChar(std::string targetString, char delimiter)
{
    targetString.erase(std::remove(targetString.begin(), targetString.end(), delimiter), targetString.end());

    return targetString;
}

// Function to Split a string with another string - returns a vector split by the inserted string
std::vector<std::string> SplitString(const std::string& targetString, const std::string& delimiter)
{
    size_t posStart = 0;
    size_t posEnd{ };
    size_t delimiterLength { delimiter.length() };

    std::string token;
    std::vector<std::string> targetVector;

    while ( (posEnd = targetString.find(delimiter, posStart)) != std::string::npos )
    {
        token = targetString.substr(posStart, posEnd - posStart);
        posStart = posEnd + delimiterLength;
        targetVector.push_back(token);
    }
    targetVector.push_back(targetString.substr(posStart));

    return targetVector;
}

// Function that checks Vector indexes against string for a match - returns a boolean whether if found something or not
bool VectorStringMatch(const std::vector<std::string>& targetVector, const std::string& targetString)
{
    bool isMatch = false;
    for (int i = 0; i < targetVector.size(); i++)
    {
        if (targetVector[i] == targetString)
        {
            isMatch = true;
            break;
        }
        else
        {
            continue;
        }
    }

    return isMatch;
}

// Check if a vector<std::string> has any matches within the target input string - returns a bool
bool ContainsKeyword(const std::string& input, const std::vector<std::string>& keywords)
{
    return std::any_of(keywords.begin(), keywords.end(), [&input](const std::string& keyword)
    {
        return input.find(keyword) != std::string::npos;
    });
}