//
// Created by ngejz7 on 2024-04-23.
//

#include <iostream>
#include <vector>

#ifndef GENERALPLUSTOOLS_STRINGMODFUNCTIONS_H
#define GENERALPLUSTOOLS_STRINGMODFUNCTIONS_H

std::string RemoveChar(std::string, char);
std::vector<std::string> SplitString(const std::string&, const std::string&);
bool VectorStringMatch(const std::vector<std::string>&, const std::string&);
bool ContainsKeyword(const std::string& input, const std::vector<std::string>& keywords);

#endif //GENERALPLUSTOOLS_STRINGMODFUNCTIONS_H
