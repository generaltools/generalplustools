## Adding Library of Abaqus Library data
add_library(AbaqusLibrary_Lib STATIC
        AbaqusKeywords.cpp
        AbaqusElementLibrary.cpp
)

## Including Headers
target_include_directories(AbaqusLibrary_Lib PUBLIC "./")