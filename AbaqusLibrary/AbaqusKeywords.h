//
// Created by ngejz7 on 2024-04-23.
//

#include <iostream>
#include <vector>

#ifndef GENERALPLUSTOOLS_ABAQUSKEYWORDS_H
#define GENERALPLUSTOOLS_ABAQUSKEYWORDS_H


class AbaqusKeywords
{
public:
    // All keys supported by class
    static std::vector<std::string> AbaqusAllKeys();

    // Keywords supporting base model characteristics
    static std::vector<std::string> AbaqusNodeKeys();
    static std::vector<std::string> AbaqusElementKeys();

    // Keywords supporting connectivity
    static std::vector<std::string> AbaqusCouplingKeys();

    // Keywords supporting property/section definition of elements
    static std::vector<std::string> AbaqusSectionKeys();
    static std::vector<std::string> AbaqusBehaviourKeys();

    // Keywords supporting set definitions
    static std::vector<std::string> AbaqusSetKeys();

    // Keywords supporting surface definitions
    static std::vector<std::string> AbaqusSurfaceKeys();

    // Keywords supporting material definitions
    static std::vector<std::string> AbaqusMaterialKeys();

};

#endif //GENERALPLUSTOOLS_ABAQUSKEYWORDS_H
