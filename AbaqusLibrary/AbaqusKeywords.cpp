//
// Created by ngejz7 on 2024-04-23.
//

#include "AbaqusKeywords.h"
#include <iostream>
#include <vector>


std::vector<std::string> AbaqusKeywords::AbaqusAllKeys()
{
    std::vector<std::string> abaqusAllKeys {
        "*NODE",
        "*ELEMENT",
        "*COUPLING",
        "*SURFACE",
        "*SOLID SECTION",
        "*BEAM SECTION",
        "*SHELL SECTION",
        "*CONNECTOR SECTION",
        "*CONNECTOR BEHAVIOR",
        "*CONNECTOR ELASTICITY",
        "*MASS",
        "*NSET",
        "*ELSET",
        "*ROTARY INERTIA",
        "*SURFACE INTERACTION",
        "*SURFACE BEHAVIOR",
        "*FRICTION",
        "*CONTACT PAIR",
        "*TIE",
        "*MATERIAL",
        "*ELASTIC",
        "*PLASTIC",
        "*DENSITY",
        "*EXPANSION",
        "*CONDUCTIVITY",
        "*SPECIFIC HEAT",
        "*FAIL STRESS",
        "*FAIL STRAIN",
        "*TIME POINTS",
    };

    return abaqusAllKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusNodeKeys()
{
    std::vector<std::string> abaqusNodeKeys {
        "*NODE",
    };

    return abaqusNodeKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusElementKeys()
{
    std::vector<std::string> abaqusElementKeys {
        "*ELEMENT",
    };

    return abaqusElementKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusCouplingKeys()
{
    std::vector<std::string> abaqusCouplingKeys {
        "*COUPLING",
    };

    return abaqusCouplingKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusSectionKeys()
{
    std::vector<std::string> abaqusSectionKeys {
        "*SOLID SECTION",
        "*SHELL SECTION",
        "*BEAM SECTION",
        "*CONNECTOR SECTION",
    };

    return abaqusSectionKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusBehaviourKeys()
{
    std::vector<std::string> abaqusBehaviourKeys {
        "*CONNECTOR ELASTICITY",
        "'CONNECTOR BEHAVIOR",
    };

    return abaqusBehaviourKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusSetKeys()
{
    std::vector<std::string> abaqusSetKeys {
        "*NSET",
        "*ELSET",
    };

    return abaqusSetKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusSurfaceKeys()
{
    std::vector<std::string> abaqusSurfaceKeys {
        "*SURFACE",
    };

    return abaqusSurfaceKeys;
}

std::vector<std::string> AbaqusKeywords::AbaqusMaterialKeys()
{
    std::vector<std::string> abaqusMaterialKeys {
        "*MATERIAL",
        "*ELASTIC",
        "*PLASTIC",
        "*DENSITY",
        "*EXPANSION",
        "*SPECIFIC HEAT",
        "*FAIL STRESS",
        "*FAIL STRAIN",
    };

    return abaqusMaterialKeys;
}
