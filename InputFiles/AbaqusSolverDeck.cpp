//
// Created by ngejz7 on 2024-04-23.
//

#include "AbaqusSolverDeck.h"

#include "boost/algorithm/string.hpp"
#include <fstream>
#include <filesystem>
#include <iostream>
#include "StringModFunctions.h"

// ## Abq_SolverDeck Class Start ##

Abq_SolverDeck::Abq_SolverDeck(std::string inputFile) : includeDetected{false}, mainInclude(inputFile)
{
    // setup base directories & boolean flags
    std::filesystem::path mainIncludePath(mainInclude);
    analysisDir = mainIncludePath.parent_path().string();

    includes.push_back(mainInclude);

}

void Abq_SolverDeck::Consolidate()
{
    do
    {
        includeDetected = GetIncludes();
    }
    while (includeDetected);
}

bool Abq_SolverDeck::GetIncludes()
{
    includeDetected = false;

    for ( int i = 0; i < includes.size(); i++)
    {
        std::string line;
        std::fstream include(includes[i]);

        while (getline(include, line))
        {
            if (boost::algorithm::contains(line, "*INCLUDE"))
            {
                std::string cleaned = RemoveChar(RemoveChar(RemoveChar(line, ' '), '\n'), '\r');
                std::vector cleanedSplitComma = SplitString(cleaned, ",");
                std::vector cleanedSplitEquals = SplitString(cleanedSplitComma[1], "=");

                std::filesystem::path includePath(cleanedSplitEquals[1]);
                std::string includeFullPath;

                if (includePath.has_parent_path())
                {
                    includeFullPath = cleanedSplitEquals[1];
                }
                else if (!includePath.has_parent_path())
                {
                    includeFullPath = analysisDir + "/" + cleanedSplitEquals[1];
                }

                if (!VectorStringMatch(includes, includeFullPath))
                {
                    includes.push_back(includeFullPath);
                    includeDetected = true;
                }
                else
                    continue;
            }
            else
                continue;
        }
    }

    return includeDetected;
}

std::vector<std::string> Abq_SolverDeck::getIncludes() const
{
    return includes;
}

// ## Abq_SolverDeck Class END ##

