//
// Created by NGEJZ7 on 2024-04-28.
//

#include <iostream>
#include <vector>
#include <map>

#ifndef GENERALPLUSTOOLS_PARSE4MODELDEFINITION_H
#define GENERALPLUSTOOLS_PARSE4MODELDEFINITION_H


class Parse4ModelDefinition
{
public:
    std::map<int, std::string> getIncludeLineNumbering();
    std::map<int, std::vector<std::string>> getIncludeKeywordNumbering();

};


#endif //GENERALPLUSTOOLS_PARSE4MODELDEFINITION_H
