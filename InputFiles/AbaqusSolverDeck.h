//
// Created by ngejz7 on 2024-04-23.
//

#include <iostream>
#include <vector>

#ifndef GENERALPLUSTOOLS_ABAQUSSOLVERDECK_H
#define GENERALPLUSTOOLS_ABAQUSSOLVERDECK_H


class Abq_SolverDeck
{
public:
    // Includes and consilidating supporting files of the Solver Deck
    std::string mainInclude;
    std::string analysisDir;
    std::vector<std::string> includes{ };

    explicit Abq_SolverDeck(std::string);
    void Consolidate();

    std::vector<std::string> getIncludes() const;

private:
    bool includeDetected;
    bool GetIncludes();


};

#endif //GENERALPLUSTOOLS_ABAQUSSOLVERDECK_H
