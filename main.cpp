//
// Created by ngejz7 on 2024-04-23.
//
#include <iostream>
#include "StringModFunctions.h"
#include "InputFiles/AbaqusSolverDeck.h"
#include <vector>

#include "sqlite3.h"

int main()
{
    std::cout << "This is working" << std::endl;

    std::string mainInputFile = R"(M:\_MS_BEV3\06_Simulation\001_RTLC\205_Green_2021_v28\03_ABQ\1305_MS30_2X_X1025_006-A4x2-AD4150-F950-0_2021cw28\1305_LIN_FT_130mm_BC-wheel-5thWheel_MS30_2X_X1025_006-A4x2-AD4150-F950-0_2021cw28_LIN.inp)";

    Abq_SolverDeck inputDeck(mainInputFile);
    inputDeck.Consolidate();

    std::vector<std::string> includes = inputDeck.getIncludes();

    for (const auto & include : includes)
    {
        std::cout << include << std::endl;
    }

    sqlite3 *db;
    char *zErrMsg = nullptr;
    int rc;

    rc = sqlite3_open("test.db3", &db);

    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return(0);

    }

    else
    {
        fprintf(stderr, "Opened Database Successfully\n");
    }

    sqlite3_close(db);
    
}